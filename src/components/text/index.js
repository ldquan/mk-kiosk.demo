import styled from 'styled-components';

const Text = styled.div`
  text-align: ${props => props.align ?? 'left'};
  display: ${props => props.block ?? 'block'};
  font-size: ${props => props.fontSize ?? 16}px;
  font-weight: ${props => props.fontWeight ?? 500};
  color: ${props => props.color ?? "#ffffff"};
`;

export {Text};
