import styled from 'styled-components'
import {Col, Input, Row} from 'antd'
import Image from 'next/image'
import {InstagramOutlined, FacebookOutlined} from '@ant-design/icons'

import {Clearfix, Text} from '@/components'

const HeaderLayout = styled.div`
  position: relative;
  background: #a81e24;

  & > img{
    width: 100%;
    max-height: 85vh;
    @media only screen and (max-width: 1024px) {
      max-height: 90vh;
    }
  }
`
const Overlay = styled.div`
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  width: 100%;
  padding: 0 20vw;

  @media only screen and (max-width: 1440px) {
    padding: 0 10vw;
  }

  @media only screen and (max-width: 1024px) {
    padding: 0 5vw;
  }
`

const ImageWrapper = styled.div`
  display: flex;
  align-items: center;
`
const ServiceWrapper = styled.div`
  position: absolute;
  bottom: 6vh;
  left: 0;
  right: 0;
  width: 100%;
  &>div:first-child{
    margin: 0 auto;
    width: 60vw;
    height: 30vh;
    border-radius: 4px;
    background: #ffffff;
    opacity: 0.8;
    @media only screen and (max-width: 1440px) {
      height: 28vh;
    }

    @media only screen and (max-width: 1024px) {
      height: 25vh;
    }
  }
`

const Service = styled(Col)`
  &>div{
    flex-direction: column;
  }

  img{
    @media only screen and (max-width: 1440px) {
      width: 80%
    }

    @media only screen and (max-width: 1024px) {
      width: 60%
    }
  }
`

const i18n = {
  login: 'LOGIN',
  service: 'Cyprus\' #1 Kiosk delivery service',
  question: 'Hi! What can we bring you today?',
  inputAddress: 'Please Enter Your Address',
  getall: 'Get all your kiosk and and small shopping needs dilivered straight away!',
  howto: 'How do you order? Easy!',
  steps: {
    input: 'Enter Your Address',
    choose: 'Choose Items & Checkout',
    dilivered: 'Dilivered Straight Away!',
  }
}

const iconStyle = {
  color: '#ffffff',
  fontSize: 30,
  marginRight: 10,
  cursor: 'pointer'
}

const Header = () => (
  <HeaderLayout>
    <img src="/images/header-bg-main.png" />
    <Overlay>
      <Row justify="space-between">
        <Col>
          <InstagramOutlined style={iconStyle} />
          <FacebookOutlined style={iconStyle} />
        </Col>
        <Col>
          <Text fontSize={18}>{i18n.login}</Text>
        </Col>
      </Row>
      <Clearfix height={50} />
      <Row justify="end" gutter={10} align="middle">
        <Col>
          <ImageWrapper>
            <Image src="/images/mrkiosk-1st-badge.png" width={34} height={34} layout="fixed" />
          </ImageWrapper>
        </Col>
        <Col>
          <Text fontSize={14}>{i18n.service}</Text>
        </Col>
      </Row>
      <Clearfix height={15} />
      <Row justify="end" gutter={10} align="middle">
        <Col>
          <ImageWrapper>
            <img src="/images/mrkiosk-logo-main-title.png" />
          </ImageWrapper>
        </Col>
      </Row>
      <Clearfix height={30} />
      <Row justify="end" gutter={10} align="middle">
        <Col>
          <Text fontSize={20}>{i18n.question}</Text>
        </Col>
      </Row>
      <Clearfix height={15} />
      <Row justify="end" gutter={10} align="middle">
        <Col span={9}>
          <Input size="large" block placeholder={i18n.inputAddress} />
        </Col>
      </Row>
      <Clearfix height={15} />
      <Row justify="end" gutter={10} align="middle">
        <Col span={10}>
          <Text fontSize={16} align='center' display="inline-flex">{i18n.getall}</Text>
        </Col>
      </Row>
    </Overlay>
    <ServiceWrapper>
      <Row justify="space-around" align="middle">
        <Col span={24}>
          <Text fontSize={25} fontWeight={600} color="#931832" align="center">{i18n.howto}</Text>
        </Col>
        {[
          {
            text: i18n.steps.input,
            img: '/images/mrkiosk-box1-enter-address.png',
          },
          {
            text: i18n.steps.choose,
            img: '/images/mrkiosk-box2-order.png',
          },
          {
            text: i18n.steps.dilivered,
            img: '/images/mrkiosk-box3-delivery.png',
          },
        ].map(item => (
          <Service xs={14} md={8} key={item.text}>
            <Row align="middle" gutter={[20, 20]}>
              <Col>
                <img src={item.img} alt="" />
              </Col>
              <Col>
                <Text color="#000000">{item.text}</Text>
              </Col>
            </Row>
          </Service>
        ))}
      </Row>
    </ServiceWrapper>
  </HeaderLayout>
)

export {Header}
